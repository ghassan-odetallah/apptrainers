package com.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class NewTest1 extends Initiate {

    @Test(description = "this test case is for search google ")
    public void googleTest() throws InterruptedException {
        GoogleSearch googleSearch = new GoogleSearch(driver);
        googleSearch.googleTest();

        WebElement element = driver.findElement(By.xpath("ghassam"));
        driver.manage().timeouts().implicitlyWait(9, TimeUnit.SECONDS);

        WebDriverWait wait = new WebDriverWait(driver, 66);
        wait.until(ExpectedConditions.visibilityOf(element));

    }
}