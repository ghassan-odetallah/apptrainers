package com.examples;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleSearch extends Initiate{

    @FindBy (xpath = "//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input")
    WebElement search;
    @FindBy (xpath = "//div[@class='FPdoLc tfB0Bf']//input[@name='btnK']")
    WebElement searchButton;

    public GoogleSearch(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }
    public void googleTest() {

        this.search.click();
        this.search.sendKeys("new search ");
        this.searchButton.click();
}
}