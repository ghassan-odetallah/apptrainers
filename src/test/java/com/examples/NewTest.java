package com.examples;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
public class NewTest {

    private WebDriver driver;
    @Test
    public void googleTest() {

        driver.navigate().to("https://www.google.com/");
        driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input"));
        driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[1]/div/div[2]/input"))
                .sendKeys("ghassan");
        driver.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div[1]/div[3]/center/input[1]")).click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement linkTitle = driver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div[2]/div/div[4]/" +
                "div[1]/div[1]/div/div/a[1]/div/div/span"));
        String linkTitleText = linkTitle.getText();
        Assert.assertEquals(linkTitleText,"Tawfiq & Ghassan Khoury Jewelryoo");
    }
    @BeforeTest
    public void beforeTest() {

        System.setProperty("webdriver.chrome.driver", "/Users/winfooz/Documents/workspace/apptrainers/" +
                "src/main/resources/chromedriver");
        driver=new ChromeDriver();

    }
    @AfterTest
    public void afterTest() {
        driver.quit();
    }
}