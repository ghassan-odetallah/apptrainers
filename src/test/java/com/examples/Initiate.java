package com.examples;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Initiate {

    public static WebDriver driver;

    @BeforeTest
    public void beforeTest() {

        System.setProperty("webdriver.chrome.driver",
                "/Users/winfooz/Documents/workspace/apptrainers/src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.navigate().to("https://www.google.com");
    }

    @AfterTest
    public void afterTest()  {
        driver.quit();
    }
}